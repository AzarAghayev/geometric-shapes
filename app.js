var eded = "";
var geometric = "";
var str = "";
var str2 = ""; 
var i, z, j, db;
db = 10;

var Romb = [
    R1 = function () {
        for (j = 1; j <= db; j++) {
            for (i = 0; i <= db - j + 1; i++) {
                str += ".";
                //+= " ";
            }
            if (i = db - j + 1) {
                str += "* ";
                //+= "*&nbsp&nbsp";
            }
            for (z = 1; z <= j; z++) {
                str += "  ";
                //+= "&nbsp";
            }
            str += "*";
            console.log(str);
            str = "";
            console.log();
            //+= " *";
        }

        for (j = db; j >= 1; j--) {
            for (i = db - j + 1; i >= 0; i--) {
                str += ".";
                //+= " ";
            }
            if (i = db - j + 1) {
                str += "* ";
                //+= "*&nbsp&nbsp";
            }
            for (z = j; z >= 1; z--) {
                str += "  ";
                //+= "&nbsp";
            }
            str += "*";
            console.log(str);
            str = "";
            console.log(); //+= " *";
        }
    },
    R2 = function () {
        for (j = 1; j <= db; j++) {
            for (i = 0; i <= db - j + 1; i++) {
                str += ".";
                //+= " ";
            }
            if (i = db - j + 1) {
                str += "/";
                //+= "/";
            }
            for (z = 1; z < j + 1; z++) {
                if (j < db) {
                    str += "- ";
                    //+= " &nbsp ";
                }
            }
            if (j == db) {
                for (var y = 0; y < db; y++) {
                    str += "* ";
                    //+= "*";
                }
            }
            str += "\\";
            console.log(str);
            str = "";
            console.log();
            //+= "\\";
        }

        for (j = db; j >= 1; j--) {
            for (i = db - j + 1; i >= 0; i--) {
                str += ".";
                //+= " ";
            }
            if (i = db - j + 1) {
                str += "\\";
                //+= "\\";
            }
            for (z = j; z >= 1; z--) {
                str += "- ";
                //+= " &nbsp ";
            }
            str += "/";
            console.log(str);
            str = "";
            console.log();
            //+= "/";
        }
    },
    R3 = function () {
        for (j = 1; j <= db; j++) {
            for (i = 0; i <= db - j + 1; i++) {
                str += ".";
                //+= " ";
            }
            if (i = db - j + 1) {
                str += "/";
                //+= "/";
            }
            if (j % 2 == 0) {
                for (z = 1; z <= j; z++) {
                    str += "- ";
                    //+= " &nbsp ";
                }
            }
            else {
                for (z = 1; z <= j; z++) {
                    if (j != 1) {
                        if (z == Math.ceil(j / 2)) {
                            str += "* ";
                            //+= " * ";
                        }
                        else {
                            str += "+ ";
                            //+= " &nbsp ";
                        }
                    }
                    else {
                        str += "* ";
                    }
                }
            }
            str += "\\";
            console.log(str);
            str = "";
            console.log();
            //+= "\\";
        }

        for (j = db; j >= 1; j--) {
            for (i = db - j + 1; i >= 0; i--) {
                str += ".";
                //+= " ";
            }
            if (i = db - j + 1) {
                str += "\\";
                //+= "\\";
            }
            if (j % 2 == 0) {
                for (z = j; z >= 1; z--) {
                    str += "- ";
                    //+= " &nbsp ";
                }
            }
            else {
                for (z = j; z >= 1; z--) {
                    if (j != 1) {
                        if (z == Math.ceil(j / 2)) {
                            str += "* ";
                            //+= " * ";
                        }
                        else {
                            str += "+ ";
                            //+= " &nbsp ";
                        }
                    }
                    else {
                        str += "* ";
                    }
                }
            }
            str += "/";
            console.log(str);
            str = "";
            console.log();
            //+= "/";
        }
    },
    R4 = function () {
        for (j = 1; j <= db; j++) {
            for (i = 0; i <= db - j + 1; i++) {
                str += ".";
                //+= " ";
            }
            if (i = db - j + 1) {
                str += "/ ";
                //+= "/";
            }
            for (z = 1; z <= j; z++) {
                str += "* ";
                //+= " * ";
            }
            str += "\\";
            console.log(str);
            str = "";
            console.log();
            //+= "\\";
        }

        for (j = db; j >= 1; j--) {
            for (i = db - j + 1; i >= 0; i--) {
                str += ".";
                //+= " ";
            }
            if (i = db - j + 1) {
                str += "\\ ";
                //+= "\\";
            }
            for (z = j; z >= 1; z--) {
                str += "* ";
                //+= " * ";
            }
            str += "/";
            console.log(str);
            str = "";
            console.log();
            //+= "/";
        }
    }
];

var Kvadrat = [
    K1 = function () {
        for (j = 1; j <= db; j++) {
            if (j == 1) {
                for (i = 0; i < db; i++) {
                    str2 += "* ";
                    //+= "* ";
                }
            }
            if (j > 1 && j < db) {
                for (i = 1; i <= db; i++) {
                    if (i == 1 || i == db) {
                        str2 += "* ";
                        //+= "* ";
                    }
                    else {
                        str2 += "  ";
                        //+= "+ ";
                    }
                }
            }
            if (j == db) {
                for (i = 1; i <= db; i++) {
                    str2 += "* ";
                    //+= "* ";
                }
            }
            console.log(str2);
            str2 = "";
        }
    },
    K2 = function () {
        for (j = 1; j <= db; j++) {
            if (j == 1) {
                for (i = 0; i < db; i++) {
                    str2 += "* ";
                    //+= "* ";
                }
            }
            if (j > 1 && j < db) {
                if (j == Math.ceil(db / 2)) {
                    if (db % 2 != 0) {
                        for (i = 1; i <= db; i++) {
                            if (i == 1 || i == db) {
                                str2 += "* ";
                                //+= "* ";
                            }
                            else {
                                str2 += "- ";
                                //+= "+ ";
                            }
                        }
                    }
                    else {
                        for (i = 1; i <= db; i++) {
                            if (i == 1 || i == db) {
                                str2 += "* ";
                                //+= "* ";
                            }
                            else {
                                str2 += "+ ";
                                //+= "+ ";
                            }
                        }
                    }
                }
                else {
                    for (i = 1; i <= db; i++) {
                        if (i == 1 || i == db) {
                            str2 += "* ";
                            //+= "* ";
                        }
                        else {
                            str2 += "+ ";
                            //+= "+ ";
                        }
                    }
                }
            }
            if (j == db) {
                for (i = 1; i <= db; i++) {
                    str2 += "* ";
                    //+= "* ";
                }
            }
            console.log(str2);
            str2 = "";
        }
    },
    K3 = function () {
        for (j = 1; j <= db; j++) {
            if (j == 1) {
                for (i = 1; i <= db; i++) {
                    str2 += "* ";
                    //+= "* ";
                }
            }
            if (j > 1 && j < db) {
                for (i = 1; i <= db; i++) {
                    if (i == Math.ceil(db / 2)) {
                        if (db % 2 != 0) {
                            str2 += "* ";
                        }
                        else {
                            str2 += "+ ";
                        }
                    }
                    if (i == 1 || i == db) {
                        str2 += "* ";
                        //+= "* ";
                    }
                    else if (i != Math.ceil(db / 2)) {
                        str2 += "+ ";
                        //+= "+ ";
                    }
                }
            }
            if (j == db) {
                for (i = 1; i <= db; i++) {
                    str2 += "* ";
                    //+= "* ";
                }
            }
            console.log(str2);
            str2 = "";
        }
    },
    K4 = function () {
        for (j = 1; j <= db; j++) {
            if (j == 1) {
                for (i = 0; i < db; i++) {
                    str2 += "- ";
                    //+= "* ";
                }
            }
            if (j > 1 && j < db) {
                for (i = 1; i <= db; i++) {
                    if (i == 1 || i == db) {
                        str2 += "- ";
                        //+= "* ";
                    }
                    else {
                        str2 += "* ";
                        //+= "+ ";
                    }
                }
            }
            if (j == db) {
                for (i = 1; i <= db; i++) {
                    str2 += "- ";
                    //+= "* ";
                }
            }
            console.log(str2);
            str2 = "";
        }
    }
];

var readline = require('readline');
var rl = readline.createInterface(process.stdin, process.stdout);
var fun = function () {
    rl.question("Bir edet daxil edin! ", function (answer) {
        db = answer;
        rl.question("Kvadrat yoxsa Romb? ", function (answer) {
            geometric = answer;
            if (geometric == "Kvadrat") {
                for (var k = 0; k < 4; k++) {
                    Kvadrat[k]();
                }
            }
            else if (geometric == "Romb") {
                for (var r = 0; r < 4; r++) {
                    Romb[r]();
                }
            }
            else {
                console.log("Kvadrat ve ya Romb yazin!!!");
                console.log("Program yeniden baslayacaq...");
                fun();
            }
        });
    });
}
fun();
